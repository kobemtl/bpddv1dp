FROM openjdk:8-jdk-alpine
#VOLUME /volumes/jenkins_volume/_data/workspace/Guests-Manager\ pipeline\@2/target
COPY /var/jenkins_home/workspace/Gu*/target/guests-manager-0.0.1-SNAPSHOT.jar guests-manager.jar 
ENTRYPOINT ["java","-jar","/guests-manager.jar"]