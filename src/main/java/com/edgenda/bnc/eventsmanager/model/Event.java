package com.edgenda.bnc.eventsmanager.model;
// Hello. // 
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
public class Event {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @NotEmpty
    private String evtdate;

    @ManyToMany(mappedBy = "events")
    @JsonIgnoreProperties({"events", "id", "description", "evtdate"})
    private List<Guest> guests;

    public Event() {
    }

    public Event(Long id) {
        this.id = id;
    }

    @PersistenceConstructor
    public Event(Long id, String name, String description, String evtdate, List<Guest> guests) {
       this.id = id;
       this.name = name;
       this.description = description;
        this.evtdate = evtdate;
        this.guests = guests;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getEvtdate() {
        return evtdate;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    @PreRemove
    private void removeGuestsFromEvent() {
        for (Guest guest : guests) {
            guest.getEvents().remove(this);
        }
    }

}
