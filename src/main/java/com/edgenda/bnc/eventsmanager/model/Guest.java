package com.edgenda.bnc.eventsmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
public class Guest {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 1)
    private String firstName;

    @NotNull
    @Size(min = 1)
    private String lastName;

    @Email
    @NotNull
    private String email;

    @ManyToMany
        @JoinTable(
            name = "EVENTS_GUESTS",
            joinColumns = { @JoinColumn(name = "event_id", referencedColumnName="id") },
            inverseJoinColumns = { @JoinColumn(name = "guest_id", referencedColumnName="id") }
    )


    @JsonIgnoreProperties("guests")
    private List<Event> events;

    public Guest() {
    }

    @PersistenceConstructor
    public Guest(Long id, String firstName, String lastName, String email, List<Event> events) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.events = events;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public List<Event> getEvents() {
        return events;
    }
}
