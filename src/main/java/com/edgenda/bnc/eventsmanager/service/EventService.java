package com.edgenda.bnc.eventsmanager.service;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;
import com.edgenda.bnc.eventsmanager.repository.EventRepository;
import com.edgenda.bnc.eventsmanager.repository.GuestRepository;
import com.edgenda.bnc.eventsmanager.service.exception.UnknownEventException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class EventService {

    private final EventRepository eventRepository;

    private final GuestRepository guestRepository;

    @Autowired
    public EventService(EventRepository eventRepository, GuestRepository guestRepository) {
        this.eventRepository = eventRepository;
        this.guestRepository = guestRepository;
    }

    public Event getEvent(Long id) {
        Assert.notNull(id, "Event ID cannot be null");
        return eventRepository.findById(id)
                .orElseThrow(() -> new UnknownEventException(id));
    }

    public List<Event> getEvents() {
        return eventRepository.findAll();
    }

    public List<Event> getEventsByDate(String dateparam) {
        return eventRepository.findByEvtdate(dateparam);
    }


    public Event createEvent(Event event) {
        Assert.notNull(event, "Event cannot be null");
        final Event newEvent = new Event(
                event.getId(),
                event.getName(),
                event.getDescription(),
                event.getEvtdate(),
                Collections.emptyList()
        );
        return eventRepository.save(newEvent);
    }

    public void updateEvent(Event event) {
        Assert.notNull(event, "Event cannot be null");
        this.getEvent(event.getId());
        eventRepository.save(event);
    }

    public List<Guest> getEventGuests(Long eventId) {
        return guestRepository.findByEventId(eventId);
    }

    public void deleteEvent(Long id) {
        Assert.notNull(id, "ID cannot be null");
        eventRepository.delete(id);
    }
}
