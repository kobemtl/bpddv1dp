package com.edgenda.bnc.eventsmanager.service.exception;

public class UnknownGuestException extends RuntimeException {

    public UnknownGuestException(Long id) {
        super("Unknown Guest with ID=" + id);
    }

}
